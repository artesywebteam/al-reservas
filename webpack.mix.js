let mix = require('laravel-mix');

mix.autoload({
    jquery: ['$','window.$','window.jQuery','jQuery'],
    vue: ['Vue','window.Vue'],
    moment: ['moment','window.moment'],
    axios: ['axios','window.axios'],
    'eonasdan-bootstrap-datetimepicker': ['$.fn.datetimepicker'],
    'validate.js': ['validate','window.validate']
});

mix.setPublicPath('./public');

//JavaScript
mix.js('resources/assets/js/app.js','public/js/app.js');
mix.extract(['vue','jquery','axios','moment','eonasdan-bootstrap-datetimepicker']);

//css
mix.sass('resources/assets/sass/vendor.scss','public/css/vendor.css');
mix.sass('resources/assets/sass/app.scss','public/css/app.css');

//copy
// mix.copy('resources/assets/js/vars.js','public/js');
mix.copy('node_modules/bootstrap-sass/assets/fonts/bootstrap/','public/fonts/bootstrap');

if(mix.inProduction()){
    // mix.version();
}
else{
    mix.browserSync('localhost:8000');
}
