
// window._ = require('lodash');
// window.Popper = require('popper.js').default;

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */



/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


/**
  * moment.js
  *
  **/
require('moment/locale/es.js');
window.moment.locale("es");

/**
 * moment-range.js
 */
const MomentRange = require('moment-range');
window.moment = MomentRange.extendMoment(moment);

/**
  * bootstrap datetimepicker
  *
  **/
// $.fn.datetimepicker = require('eonasdan-bootstrap-datetimepicker');


// Extend the default Number object with a formatMoney() method:
// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
// defaults: (2, "$", ",", ".")
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
	places = !isNaN(places = Math.abs(places)) ? places : 2;
	symbol = symbol !== undefined ? symbol : "$";
	thousand = thousand || ",";
	decimal = decimal || ".";
	var number = this,
	    negative = number < 0 ? "-" : "",
	    i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
	    j = (j = i.length) > 3 ? j % 3 : 0;
	return "$ " + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "") + " " + symbol;
};


/**
  * getQueryVariable
  **/
function getQueryVariable(variable){
     var query = window.location.search.substring(1);
     var vars = query.split("&");
     for (var i=0;i<vars.length;i++) {
             var pair = vars[i].split("=");
             if(pair[0] == variable){return pair[1];}
     }
     return(false);
}

/**
  * Vue directives
  *
  **/

Vue.directive("datetime", {
	twoWay: true,
	bind (el, binding, vnode){
		let fecha_options = {
	            locale: 'es',
	            showClose: true,
	            format: 'DD-MM-YYYY',
	            ignoreReadonly: true,
	            widgetPositioning: { horizontal: 'auto', vertical: 'bottom' },
	            icons: { close: 'glyphicon glyphicon-ok' },
	            minDate: moment().startOf('date'),
				useCurrent: false
	    };

		let hora_options = {
	            locale: 'es',
	            format: 'H:mm',
	            stepping: 15,
	            ignoreReadonly: true,
	            widgetPositioning: { horizontal: 'auto', vertical: 'bottom' },
	            icons: { close: 'glyphicon glyphicon-ok' },
				useCurrent: false
	    };

		let format = "";


		if(binding.arg == 'time'){
			jQuery(el).datetimepicker(hora_options);
			format = hora_options.format;
		}
		else if (binding.arg == 'date') {
			jQuery(el).datetimepicker(fecha_options);
			format = fecha_options.format;
		}

		jQuery(el).on('dp.change', function(ev){
			var event = new Event('input', {bubbles: true});
			if(ev.hasOwnProperty('date'))
				el.value = ev.date.format(format);

			el.dispatchEvent(event);
		});
	}
})
