class Car {
    constructor(data) {
        this.id = data.id;
        this.value = data.value;
        this.name = data.name;
        this.description = data.description;
        this.image = data.image;
        this.prices = data.prices;
        this.extra_hours = data.extra_hours;

        // new attributes
        this.category = data.category;
        this.anuncio = data.anuncio;
        this.descuento = data.descuento;
        this.precio_temporada = data.precio_temporada;
        this.agotado = data.agotado;
        this.esta_en_temporada = false;

    }

    getFullName(){
        return `${this.name} (${this.category})`;
    }

    getDayPrice(){
        let price = (this.esta_en_temporada) ? this.getSeasonPrice() : this.prices[0];
        return parseInt(price);
    }

    getWeekPrice(){
        return parseInt(this.prices[1]);
    }

    getMonthPrice(){
        return parseInt(this.prices[2]);
    }

    getAccumulatedDaysPrice(days){
        let accumulated_price = 0;
        let current_price = 0;
        let remainder_thirtydays = days % 30;
        
        
        if (Math.floor(days / 30) > 0) {
            //get accumulated price 
            accumulated_price = parseInt(Math.floor(days / 30) * this.getMonthPrice())
        }

        if(remainder_thirtydays < 7){
            current_price = this.getDayPrice() * remainder_thirtydays;
        }
        else if(remainder_thirtydays >= 7){
            let weekly_current_price  = this.getWeekPrice() * remainder_thirtydays;
            let month_current_price  = this.getMonthPrice();

            current_price = Math.min(
                weekly_current_price,
                month_current_price
            );
        }

        /**
         * cuando esta en una temporada alta, no se da descuento. 
         * Se multiplica el precio de temporada diario del vehiculo 
         * por el número de vehiculos
         *  */ 
        if(this.esta_en_temporada){
            accumulated_price = 0;
            current_price = this.getSeasonPrice() * days;
        }
        

        return parseInt(accumulated_price + current_price);
    }

    getSubtotalPrice(days){
        return parseInt(this.getDayPrice() * days);
    }

    getDiscountPrice(days){
        return parseInt(this.getSubtotalPrice(days) - this.getAccumulatedDaysPrice(days));
    }

    getExtraHoursPrice(hours){
        if(hours >= 1 && hours <= 4) {
             return parseInt(this.extra_hours[hours]);
        }
        else{
            return 0;
        }
    }

    getTotalPrice(days, hours){
        return this.getAccumulatedDaysPrice(days) + this.getExtraHoursPrice(hours);
    }

    getSeasonPrice(){
        try {
            return this.precio_temporada[0];
        } catch (error) {
            return 0;
        }
    }

    getSeasonInitDate(){
        try {
            return this.precio_temporada[1];
        } catch (error) {
            return false;
        }
    }

    getSeasonEndDate(){
        try {
            return this.precio_temporada[2];
        } catch (error) {
            return false;
        }
    }



    toString(){
        return this.value
    }
}

export default Car;
