window.validate = require('validate.js');
validate.validators.presence.message = "Este campo es requerido";
validate.validators.length.tooShort = "Debe tener mínimo 5 caracteres";

import Form from './classes/Form.js';
import Car from './classes/Car.js';

let cs = [];
window.vehiculos.forEach((c) => {
    cs.push(new Car(c));
})


let automate_page = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port: '');
let page = (paginaweb) ? paginaweb : automate_page;

let constraints = {
    nombre: {
        presence: {
            allowEmpty: false,
        },
        length: {
            minimum: 5
        }
    },
    identificacion: {
        presence: {
            allowEmpty: false,
        },
        format: {
            pattern: "[a-z0-9]+",
            flags: "i",
            message: "Solo puede contener caractéres numéricos y alfabéticos [0-9][a-z]",
        },
        length: {
            minimum: 5
        }
    },
    telefono: {
        presence: {
            allowEmpty: false,
        },
        format: {
            pattern: "^[\+]?[0-9]+",
            flags: "i",
            message: "Solo puede contener caractéres numéricos [0-9] y \"+\" al inicio",
        },
        length: {
            minimum: 6,
            message: "Debe tener mínimo 6 caracteres"
        }
    },
    email: {
        presence: {
            allowEmpty: false,
            
        },
        email: {
            message: "No es un correo válido"
        }
    },
    vehiculo: {
        presence: {
            allowEmpty: false,
        }
    },
    lugar_recogida: {
        presence: {
            allowEmpty: false,
        }
    },
    fecha_recogida: {
        presence: {
            allowEmpty: false,
        }
    },
    hora_recogida: {
        presence: {
            allowEmpty: false,
        }
    },
    lugar_devolucion: {
        presence: {
            allowEmpty: false,
        }
    },
    fecha_devolucion: {
        presence: {
            allowEmpty: false,
        }
    },
    hora_devolucion: {
        presence: {
            allowEmpty: false,
        }
    },
    politica_privacidad: {
        presence: {
            allowEmpty: false,
        }

    },
};

let form_fields = {
    nombre: null,
    identificacion: null,
    telefono: null,
    fecha_nacimiento: null,
    direccion_residencia: null,
    ciudad_residencia: null,
    email: null,
    aerolinea: null,
    numero_vuelo_ida: null,
    numero_vuelo_regreso: null,
    referencia_nombre: null,
    referencia_movil: null,
    vehiculo: null,
    lugar_recogida: null,
    fecha_recogida: null,
    hora_recogida: '10:00',
    lugar_devolucion: null,
    fecha_devolucion: null,
    hora_devolucion: '10:00',
    url: page,
    politica_privacidad: null,
    seguro_proteccion_total: false,
    dias_cotizados: 0,
    horas_extras: 0,
    precio_dia_carro: 0,
    precio_horas_extras: 0,
    precio_subtotal: 0,
    precio_descuento: 0,
    precio_total: 0,

};

let vue_data = {
    cars: cs,
    selected_days: null,
    selected_hours: null,
    pick_up_places: sedes,
    have_flight: false,
    form: new Form(form_fields, constraints, window.validate),
    submitingError: null,

};

export default {
    constraints,
    form_fields,
    vue_data
}
