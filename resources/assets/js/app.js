require('./bootstrap');

import Vars from './basevars.js';
import Car from './classes/Car.js';

const app = new Vue({
    el: '#formulario',
    data: Vars.vue_data,
    computed: {
        formatMoneySubtotalPrice(){
            if(this.form.vehiculo && this.selected_days){
                this.form.precio_subtotal = this.form.vehiculo.getSubtotalPrice(this.selected_days);
                return this.form.precio_subtotal.formatMoney(0, "COP");
            }

        },
        formatMoneyTotalPrice(){
            if(this.form.vehiculo && this.selected_days){
                this.form.precio_total = this.form.vehiculo.getTotalPrice(this.selected_days, this.selected_hours);
                return this.form.precio_total.formatMoney(0, "COP");
            }

        },
        formatMoneyDiscountPrice(){
            if(this.form.vehiculo && this.selected_days){
                this.form.precio_descuento = this.form.vehiculo.getDiscountPrice(this.selected_days);
                return this.form.precio_descuento.formatMoney(0, "COP");
            }
            else{
                this.form.precio_descuento = null;
            }

        },
        formatMoneyPricePerDay(){
            if(this.form.vehiculo && this.selected_days){
                this.form.precio_dia_carro = this.form.vehiculo.getDayPrice();
                return this.form.precio_dia_carro.formatMoney(0, "COP");
            }
            else {
                this.form.precio_dia_carro = null;
            }

        },
        formatMoneyExtraHours(){
            if(this.form.vehiculo && this.selected_hours){
                this.form.precio_horas_extras = this.form.vehiculo.getExtraHoursPrice(this.selected_hours);
                return this.form.precio_horas_extras.formatMoney(0, "COP");
            }
            else{
                this.form.precio_horas_extras = null;
            }

        },
        formatHourDifference(){
            if(this.selected_hours){
                let plural = (this.selected_hours > 1) ? 'Horas' : 'Hora';
                return String(this.selected_hours) + " " + plural;
            }
        },
        formatTotalInsurancePrice(){
            if(seguroprotecciontotal){
                return seguroprotecciontotal.formatMoney(0, "COP");
            }
        },
        filterUnabledCarCategories(){
            return this.cars.filter(function(car) {
                return car.agotado == false;
            });
        }

    },
    mounted() {
        let car_from_external = this.externalVar("v");
        if(car_from_external) this.form.vehiculo = this.carFromId(car_from_external);

        $("#fecha_recogida_picker").on("dp.change", function (e) {
            $('#fecha_devolucion_picker').data("DateTimePicker").minDate(e.date);
        });
        $("#fecha_devolucion_picker").on("dp.change", function (e) {
            $('#fecha_recogida_picker').data("DateTimePicker").maxDate(e.date);
        });
    },
    methods: {
        carFromId(id){
            var result = vehiculos.filter(vehiculo => vehiculo.id == id);
            return (result.length > 0) ? new Car(result[0]) : { name: '' };
        },
        externalVar(variable){
               var query = window.location.search.substring(1);
               var vars = query.split("&");
               for (var i=0;i<vars.length;i++) {
                       var pair = vars[i].split("=");
                       if(pair[0] == variable){return pair[1];}
               }
               return(false);
        },
        onSubmit(){
            this.form.validateData();
            if(!this.form.errors.any()){
                let endpoint =
                (process.env.NODE_ENV === 'production') ? 'https://iz590xje3m.execute-api.us-west-2.amazonaws.com/prod/leads' : 'http://localhost:7070/subscribe/formlead';

                this.form.post(endpoint)
                .then(data => {
                    console.log('POST OK.');
                    window.location.replace("gracias.html");
                })
                .catch(errors => {
                    console.log('POST Failed.');
                    this.submitingError = "Hubo un error al enviar la información. Por favor intentelo de nuevo.";
                })
            }
        },
        toggleFechaRecogidaButton(){
            jQuery('#fecha_recogida_picker').data('DateTimePicker').toggle();
        },
        toggleHoraRecogidaButton(){
            jQuery('#hora_recogida_picker').data('DateTimePicker').toggle();
        },
        toggleFechaDevolucionButton(){
            jQuery('#fecha_devolucion_picker').data('DateTimePicker').toggle();
        },
        toggleHoraDevolucionButton(){
            jQuery('#hora_devolucion_picker').data('DateTimePicker').toggle();
        },
        setExtraHours(){
            if(this.form.hora_recogida && this.form.hora_devolucion){
                if(this.form.fecha_recogida == this.form.fecha_devolucion){
                    this.selected_hours = null;
                    return;
                }

                let init_hour = moment(this.form.hora_recogida,'H:mm');
                let end_hour = moment(this.form.hora_devolucion,'H:mm');

                if(end_hour.isValid() && init_hour.isValid()){
                    let hours = end_hour.diff(init_hour,'hours');
                    if(hours >= 1 && hours <= 4){
    					this.selected_hours = hours;

    				}
                    else{
                        this.selected_hours = null;
                    }
                    this.form.horas_extras = this.selected_hours;
                }
            }
        },
        checkExtraHours(){

            if(this.selected_hours && this.form.vehiculo){

                if(this.selected_hours >= 1 && this.selected_hours <= 4){

                    return true;
                }

            }

            return false;
        },
        checkInsurance(){
            if(this.form.seguro_proteccion_total){
                this.form.vehiculo.total_insurance_protection = seguroprotecciontotal;
            }
            else{
                this.form.vehiculo.total_insurance_protection = null;
            }

            this.form.seguro_proteccion_total = !this.form.seguro_proteccion_total;
        },

        updateSelectedDays(){
            if(this.form.fecha_recogida && this.form.fecha_devolucion && this.form.hora_recogida && this.form.hora_devolucion){
                let init_day = moment(this.form.fecha_recogida,'DD-MM-YYYY');
                let end_day = moment(this.form.fecha_devolucion,'DD-MM-YYYY');
                let diff_days = null;

                if(end_day.isValid() && init_day.isValid())
                    diff_days = end_day.diff(init_day,'days');

                if(diff_days == 0){
                    diff_days = 1;
                }
                else{
                    let init_hour = moment(this.form.hora_recogida,'H:mm');
                    let end_hour = moment(this.form.hora_devolucion,'H:mm');

                    if(end_hour.isValid() && init_hour.isValid()){
                        if(end_hour.diff(init_hour,'hours') > 4){
                            diff_days = diff_days + 1;
                        }
                    }
                }
                
                this.checkSeasonDates(init_day, end_day);

                this.selected_days = diff_days;
                this.form.dias_cotizados = diff_days;

            }
        },
        checkSeasonDates(init_date, end_date){
            try {
                let season_date_init = moment(this.form.vehiculo.getSeasonInitDate(),'YYYY-MM-DD');
                let season_date_end = moment(this.form.vehiculo.getSeasonEndDate(),'YYYY-MM-DD');
                
                if(season_date_init.isValid() && season_date_end.isValid() && init_date.isValid()){
                    let season_date_range = moment.range(season_date_init, season_date_end);
                    let today = moment(new Date());

                    if(season_date_range.contains(today) && season_date_range.contains(init_date)){
                        
                        this.form.vehiculo.esta_en_temporada = true;
                    }
                    else{
                        this.form.vehiculo.esta_en_temporada = false;
                    }
    
                }    
            } catch (error) {
                return false;
            }
            
        }
    },
    watch: {
        'form.vehiculo': function(value){
            this.updateSelectedDays();
            this.setExtraHours();
        },
        'form.fecha_recogida': function(value){
            let newDate = moment(value,'DD-MM-YYYY');
            newDate.add(1, "days");
            this.form.fecha_devolucion = newDate.format('DD-MM-YYYY');
            this.toggleFechaRecogidaButton();
            this.toggleFechaDevolucionButton();

            this.updateSelectedDays();
            this.setExtraHours();
        },
        'form.fecha_devolucion': function(value){
            this.updateSelectedDays();
            this.setExtraHours();
        },
        'form.hora_devolucion': function(value){
            this.updateSelectedDays();
            this.setExtraHours();
        },
        'form.hora_recogida': function(value){
            this.updateSelectedDays();
            this.setExtraHours();
            this.form.hora_devolucion = value;
        },
        'form.lugar_recogida': function(value){
            this.form.lugar_devolucion = value;
        }
    }
})
